import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/danger_button.dart';
import 'package:flutter_template/app/widgets/button/primary_button.dart';
import 'package:flutter_template/app/widgets/button/primary_circle_button.dart';
import 'package:flutter_template/app/widgets/button/primary_icon_button.dart';
import 'package:flutter_template/app/widgets/button/secondary_button.dart';
import 'package:flutter_template/app/widgets/button/secondary_circle_button.dart';
import 'package:flutter_template/app/widgets/button/warning_button.dart';
import 'package:flutter_template/app/widgets/text/primary_text_button.dart';
import 'package:flutter_template/app/widgets/text/secondary_text_button.dart';
import 'package:flutter_template/app/widgets/text/text_style.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  PrimaryButton(
                    onPressed: () {},
                    child: PrimaryTextButton(
                      title: 'Primary Button',
                      icon: Icons.apps,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SecondaryButton(
                    onPressed: () {},
                    child: SecondaryTextButton(title: 'Secondary Button'),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  WarningButton(
                    onPressed: () {},
                    child: PrimaryTextButton(
                      title: 'Warning Button',
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  DangerButton(
                    onPressed: () {},
                    child: PrimaryTextButton(
                      title: 'Danger Button',
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Primary circle button'),
                  PrimaryCircleButton(
                    icon: Icons.add,
                    onPressed: () {},
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Secondary circle button'),
                  SecondaryCircleButton(
                    onPressed: () {},
                    icon: Icons.add,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text('Icon Button button'),
                  PrimaryIconButton(
                    onPressed: () {},
                    icon: Icons.ac_unit,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Header Text',
                    style: AppTextStyle.header,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Subheader text',
                    style: AppTextStyle.subHeader,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Subheader2 text',
                    style: AppTextStyle.subHeader2,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Body text',
                    style: AppTextStyle.body,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Subheader text',
                    style: AppTextStyle.title,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Caption text',
                    style: AppTextStyle.caption,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
