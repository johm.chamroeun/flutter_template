import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_template/app/app_widget.dart';
import 'package:flutter_template/app/home/home_widget.dart';
import 'package:flutter_template/app/login/login_widget.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [];

  @override
  Widget get bootstrap => AppWidget();

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          '/',
          child: (_, __) => HomeWidget(),
        ),
        ModularRouter(
          '/login',
          child: (_, __) => LoginWidget(),
        )
      ];
}
