import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_icon_button.dart';
import 'package:flutter_template/common/index.dart';

class PrimaryIconButton extends StatefulWidget {
  final Function onPressed;
  final IconData icon;

  const PrimaryIconButton(
      {Key key, @required this.onPressed, @required this.icon})
      : super(key: key);

  @override
  _PrimaryIconButtonState createState() => _PrimaryIconButtonState();
}

class _PrimaryIconButtonState extends State<PrimaryIconButton> {
  @override
  Widget build(BuildContext context) {
    return BaseIconButton(
      onPressed: widget.onPressed,
      icon: widget.icon,
      color: kPrimaryColor,
      size: 40.0,
    );
  }
}
