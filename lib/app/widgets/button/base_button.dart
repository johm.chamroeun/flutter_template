import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/common/style.dart';

class BaseButton extends RaisedButton {
  final VoidCallback onPressed;
  final Widget child;
  final Color buttonColor;
  final Color borderColor;

  BaseButton({
    Key key,
    @required this.onPressed,
    this.child,
    this.buttonColor,
    this.borderColor,
  }) : super(
          key: key,
          onPressed: onPressed,
          child: child,
        );

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        buttonColor: buttonColor,
        buttonTheme: Theme.of(context).buttonTheme.copyWith(
              buttonColor: buttonColor,
              minWidth: 25.0,
              height: 45,
              splashColor: buttonColor.withAlpha(200),
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  color: borderColor == null ? buttonColor : borderColor,
                  width: 2.0,
                  style: BorderStyle.solid,
                ),
                borderRadius: kButtonRaduis,
              ),
            ),
      ),
      child: Builder(builder: super.build),
    );
  }
}
