import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_button.dart';

class WarningButton extends StatefulWidget {
  final Function onPressed;
  final Widget child;

  const WarningButton({Key key, @required this.onPressed, this.child})
      : super(key: key);

  @override
  _WarningButtonState createState() => _WarningButtonState();
}

class _WarningButtonState extends State<WarningButton> {
  @override
  Widget build(BuildContext context) {
    return BaseButton(
      onPressed: widget.onPressed,
      child: widget.child,
      borderColor: Colors.amber,
      buttonColor: Colors.amber,
    );
  }
}
