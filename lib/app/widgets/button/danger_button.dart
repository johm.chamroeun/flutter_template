import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_button.dart';

class DangerButton extends StatefulWidget {
  final Function onPressed;
  final Widget child;

  const DangerButton({Key key, @required this.onPressed, this.child})
      : super(key: key);

  @override
  _DangerButtonState createState() => _DangerButtonState();
}

class _DangerButtonState extends State<DangerButton> {
  @override
  Widget build(BuildContext context) {
    return BaseButton(
      onPressed: widget.onPressed,
      child: widget.child,
      borderColor: Colors.red,
      buttonColor: Colors.red,
    );
  }
}
