import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_circle_button.dart';
import 'package:flutter_template/common/index.dart';

class PrimaryCircleButton extends StatefulWidget {
  final Function onPressed;
  final IconData icon;

  const PrimaryCircleButton({Key key, @required this.onPressed, this.icon})
      : super(key: key);

  @override
  _PrimaryCircleButtonState createState() => _PrimaryCircleButtonState();
}

class _PrimaryCircleButtonState extends State<PrimaryCircleButton> {
  @override
  Widget build(BuildContext context) {
    return BaseCircleButton(
      onPressed: widget.onPressed,
      buttonColor: kPrimaryColor,
      iconColor: Colors.white,
      icon: widget.icon,
    );
  }
}
