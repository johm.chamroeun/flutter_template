import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_circle_button.dart';
import 'package:flutter_template/common/index.dart';

class SecondaryCircleButton extends StatefulWidget {
  final Function onPressed;
  final IconData icon;

  const SecondaryCircleButton({Key key, @required this.onPressed, this.icon})
      : super(key: key);

  @override
  _SecondaryCircleButtonState createState() => _SecondaryCircleButtonState();
}

class _SecondaryCircleButtonState extends State<SecondaryCircleButton> {
  @override
  Widget build(BuildContext context) {
    return BaseCircleButton(
      onPressed: widget.onPressed,
      buttonColor: Colors.white,
      iconColor: kPrimaryColor,
      borderColor: kPrimaryColor,
      icon: widget.icon,
    );
  }
}
