import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_button.dart';
import 'package:flutter_template/common/index.dart';

class SecondaryButton extends StatefulWidget {
  final Function onPressed;
  final Widget child;

  const SecondaryButton({Key key, @required this.onPressed, this.child})
      : super(key: key);

  @override
  _SecondaryButtonState createState() => _SecondaryButtonState();
}

class _SecondaryButtonState extends State<SecondaryButton> {
  @override
  Widget build(BuildContext context) {
    return BaseButton(
      onPressed: widget.onPressed,
      child: widget.child,
      borderColor: kPrimaryColor,
      buttonColor: Colors.white,
    );
  }
}
