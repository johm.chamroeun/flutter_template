import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BaseCircleButton extends StatefulWidget {
  final IconData icon;
  final Function onPressed;
  final Color buttonColor;
  final Color borderColor;
  final Color iconColor;
  final double size;

  const BaseCircleButton(
      {Key key,
      @required this.icon,
      @required this.onPressed,
      @required this.buttonColor,
      this.borderColor,
      this.iconColor,
      this.size = 45})
      : super(key: key);

  @override
  _BaseCircleButtonState createState() => _BaseCircleButtonState();
}

class _BaseCircleButtonState extends State<BaseCircleButton> {
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: widget.onPressed,
      elevation: 2.0,
      fillColor: widget.buttonColor,
      child: Icon(
        widget.icon,
        size: widget.size,
        color: widget.iconColor,
      ),
      padding: EdgeInsets.all(2.0),
      shape: CircleBorder(
        side: BorderSide(
          color: widget.borderColor == null
              ? widget.buttonColor
              : widget.borderColor,
        ),
      ),
    );
  }
}
