import 'package:flutter/material.dart';
import 'package:flutter_template/common/index.dart';

class BaseIconButton extends StatefulWidget {
  final Function onPressed;
  final IconData icon;
  final double size;
  final Color color;

  const BaseIconButton(
      {Key key,
      this.onPressed,
      @required this.icon,
      this.size = 45,
      this.color})
      : super(key: key);
  @override
  _BaseIconButtonState createState() => _BaseIconButtonState();
}

class _BaseIconButtonState extends State<BaseIconButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: widget.onPressed,
      iconSize: widget.size,
      color:
          widget.color == null ? Theme.of(context).primaryColor : widget.color,
      icon: Icon(
        widget.icon,
      ),
    );
  }
}
