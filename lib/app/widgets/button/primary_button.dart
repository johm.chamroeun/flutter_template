import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/button/base_button.dart';
import 'package:flutter_template/common/index.dart';

class PrimaryButton extends StatefulWidget {
  final Function onPressed;
  final Widget child;

  PrimaryButton({Key key, @required this.onPressed, this.child})
      : super(key: key);

  @override
  _PrimaryButtonState createState() => _PrimaryButtonState();
}

class _PrimaryButtonState extends State<PrimaryButton> {
  @override
  Widget build(BuildContext context) {
    return BaseButton(
      onPressed: widget.onPressed,
      child: widget.child,
      buttonColor: kPrimaryColor,
    );
  }
}
