import 'package:flutter/material.dart';

class AppTextButton extends StatelessWidget {
  final String title;
  final IconData icon;
  final Color color;

  const AppTextButton(
      {Key key, this.title, this.icon, this.color = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (title != null && icon != null) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [buildIcon(), SizedBox(width: 2), buildText()],
      );
    } else if (title == null && icon != null) {
      return buildIcon();
    } else if (title != null && icon == null) {
      return buildText();
    }
    return Container();
  }

  Text buildText() {
    return Text(
      title,
      style: TextStyle(
        color: color,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Icon buildIcon() {
    return Icon(
      icon,
      size: 20,
      color: color,
    );
  }
}
