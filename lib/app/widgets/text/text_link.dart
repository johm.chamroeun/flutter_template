import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_template/app/widgets/text/text_style.dart';

class TextLink extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final TextStyle style;
  const TextLink(
    this.text, {
    Key key,
    @required this.onTap,
    this.style = AppTextStyle.linkUrl,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Text(
        text,
        style: style,
      ),
    );
  }
}
