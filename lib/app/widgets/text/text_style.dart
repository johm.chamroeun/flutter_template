import 'package:flutter/material.dart';

class AppTextStyle {
  static AppTextStyle _instance;
  factory AppTextStyle() => _instance ?? new AppTextStyle._();
  AppTextStyle._();

  static const TextStyle header = TextStyle(
    fontSize: 40,
    fontWeight: FontWeight.bold,
  );

  static const TextStyle subHeader = TextStyle(
    fontSize: 30,
    fontWeight: FontWeight.w900,
  );

  static const TextStyle subHeader2 = TextStyle(
    fontSize: 25,
    fontWeight: FontWeight.w800,
  );

  static const TextStyle body = TextStyle(
    fontSize: 30,
  );

  static const TextStyle title = TextStyle(
    fontSize: 25,
  );

  static const TextStyle caption = TextStyle(
    fontSize: 20,
  );

  static const TextStyle linkUrl = TextStyle(
    color: Colors.blue,
  );
}
