import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/text/text_button.dart';

class PrimaryTextButton extends StatelessWidget {
  final String title;
  final IconData icon;
  const PrimaryTextButton({Key key, this.title, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppTextButton(
      title: title,
      icon: icon,
      color: Colors.white,
    );
  }
}
