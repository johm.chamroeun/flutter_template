import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/text/text_button.dart';
import 'package:flutter_template/common/index.dart';

class SecondaryTextButton extends StatelessWidget {
  final String title;
  final IconData icon;

  const SecondaryTextButton({Key key, this.title, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppTextButton(
      title: title,
      icon: icon,
      color: kPrimaryColor,
    );
    ;
  }
}
