import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_template/app/app_module.dart';
import 'package:flutter_template/app/home/home_widget.dart';

//Modular
void main() {
  runApp(
    ModularApp(
      module: AppModule(),
    ),
  );
}

// void main() {
//   runApp(MyApp());
// }

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(),
      home: HomeWidget(),
    );
  }
}
