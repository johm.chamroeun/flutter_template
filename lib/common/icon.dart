import 'package:flutter/material.dart';

class AppIcons {
  AppIcons._();
  static const IconData chatsIcon = Icons.bubble_chart;
  static const IconData call = Icons.phone;
  static const IconData videoCall = Icons.videocam;
  static const IconData camera = Icons.camera_alt;
  static const IconData image = Icons.image;
  static const IconData voice = Icons.settings_voice;
  static const IconData sendMessage = Icons.send;
  static const IconData sticker = Icons.image_aspect_ratio;
  static const IconData add = Icons.add;
  static const IconData search = Icons.search;
  static const IconData visibility_off = Icons.visibility_off;
  static const IconData visibility_on = Icons.visibility;
  static const IconData email = Icons.email;
  static const IconData phone = Icons.phone;
}
