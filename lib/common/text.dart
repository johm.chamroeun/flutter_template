import 'package:flutter/material.dart';
import 'package:flutter_template/app/widgets/text/text_style.dart';

class TextTitle extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  const TextTitle(this.text, {Key key, this.color = Colors.black, this.size})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: AppTextStyle.title,
    );
  }
}

class TextSubTitle extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  const TextSubTitle(this.text,
      {Key key, this.color = Colors.black38, this.size})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: this.size,
      ),
    );
  }
}
